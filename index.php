<?php
require_once __DIR__ . "/vendor/autoload.php";

use EdytaJordan\Response\JsonResponse;

$student = [
    "name" => "John Doe",
    "course" => "Software Engineering",
];
new JsonResponse('ok', '', $student);